import timeit


def fibo_naive(n):
    if n == 0 or n == 1:
        return 1
    return fibo_naive(n - 1) + fibo_naive(n - 2)


memo = {0: 1, 1: 1}


def fibo_memo(n):
    if n not in memo:
        memo[n] = fibo_memo(n - 1) + fibo_memo(n - 2)
    return memo[n]


if __name__ == '__main__':
    n = 35

    start = timeit.default_timer()
    print(fibo_naive(n))
    stop = timeit.default_timer()
    print('Naive Berechnung: ', stop - start)

    start = timeit.default_timer()
    print(fibo_memo(n))
    stop = timeit.default_timer()
    print('Mit Memoisation: ', stop - start)