import math


def primes(n):
    ls = list(range(0, n))
    ls[1] = 0
    last = math.floor(math.sqrt(n))
    for i in range(2, last):
        if ls[i] != 0:
            crossout(ls, i, n)
    return ls


def crossout(ls, x, n):
    i = x * x
    while i < n:
        ls[i] = 0
        i += x


def print_prime(x, digits):
    if x == 0:
        print(' ' * digits, end='  ')
        return
    fill = digits - len(str(x))
    print(fill * ' ' + str(x), end='  ')


def prime_grid(n):
    ls = primes(n)
    digits = math.ceil(math.log10(n-1))
    for i in range(1, n):
        print_prime(ls[i], digits)
        if i % 10 == 0:
            print('')


def prime_list(n):
    ls = primes(n)
    digits = math.ceil(math.log10(n-1))
    filtered = filter(lambda x: x > 0, ls)
    print(list(filtered))


if __name__ == '__main__':
    prime_grid(1001)